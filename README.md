# Codebase release r1.4 for CoVVVR

by Prasanth Shyamsundar, Jacob L. Scott, Stephen Mrenna, Konstantin T. Matchev, Kyoungchul Kong

SciPost Phys. Codebases 28-r1.4 (2024) - published 2024-03-04

[DOI:10.21468/SciPostPhysCodeb.28-r1.4](https://doi.org/10.21468/SciPostPhysCodeb.28-r1.4)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.28-r1.4) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Prasanth Shyamsundar, Jacob L. Scott, Stephen Mrenna, Konstantin T. Matchev, Kyoungchul Kong.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.28-r1.4](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.28-r1.4)
* Live (external) repository at [https://github.com/crumpstrr33/covvvr/tree/v1.4.2](https://github.com/crumpstrr33/covvvr/tree/v1.4.2)
